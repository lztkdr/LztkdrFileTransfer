﻿namespace Lztkdr.FileTransfer.Event
{
    public class OnBigStartEventArgs
    {
        public object UserState { get; set; }

        public OnBigStartEventArgs(object userState)
        {
            this.UserState = userState;
        }

        public OnBigStartEventArgs()
        {

        }
    }
}
