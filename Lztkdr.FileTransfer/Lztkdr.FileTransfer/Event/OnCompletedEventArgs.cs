﻿namespace Lztkdr.FileTransfer.Event
{
    public class OnCompletedEventArgs
    {
        public object UserState { get; set; }

        /// <summary>
        /// Md5检验结果
        /// </summary>
        public bool? CheckResult { get; set; }

        public OnCompletedEventArgs(object userState, bool? checkResult)
        {
            this.UserState = userState;
            this.CheckResult = checkResult;
        }
    }
}
