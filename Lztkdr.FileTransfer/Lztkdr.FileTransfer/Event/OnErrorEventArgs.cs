﻿using System;

namespace Lztkdr.FileTransfer.Event
{
    public class OnErrorEventArgs
    {
        public string ActionName { get; }

        public object UserState { get; }

        public Exception Exception { get; }

        public OnErrorEventArgs(string actionName, Exception exception, object userState)
        {
            this.ActionName = actionName;
            this.Exception = exception;
            this.UserState = userState;
        }
    }
}
