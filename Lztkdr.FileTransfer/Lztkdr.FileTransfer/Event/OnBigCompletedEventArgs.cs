﻿namespace Lztkdr.FileTransfer.Event
{
    public class OnBigCompletedEventArgs
    {
        public object UserState { get; set; }

        public OnBigCompletedEventArgs(object userState)
        {
            this.UserState = userState;
        }
    }
}
