﻿namespace Lztkdr.FileTransfer.Event
{
    public class OnStartEventArgs
    {
        public object UserState { get; set; }

        public OnStartEventArgs(object userState)
        {
            this.UserState = userState;
        }
    }
}
