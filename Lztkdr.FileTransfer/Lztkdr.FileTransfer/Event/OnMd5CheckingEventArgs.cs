﻿namespace Lztkdr.FileTransfer.Event
{
    public class OnMd5CheckingEventArgs
    {
        public object UserState { get; set; }

        public OnMd5CheckingEventArgs(object userState)
        {
            this.UserState = userState;
        }
    }
}
