﻿namespace Lztkdr.FileTransfer.Event
{
    public class OnBigUpdateProgressEventArgs
    {
        public object UserState { get; set; }

        public OnBigUpdateProgressEventArgs(object userState)
        {
            this.UserState = userState;
        }
    }
}
