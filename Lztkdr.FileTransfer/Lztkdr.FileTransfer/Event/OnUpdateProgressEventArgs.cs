﻿namespace Lztkdr.FileTransfer.Event
{
    public class OnUpdateProgressEventArgs
    {
        public object UserState { get; set; }

        public decimal Rate { get; set; }

        public OnUpdateProgressEventArgs(object userState, decimal rate)
        {
            this.UserState = userState;
            this.Rate = (rate >= 1 ? 0.990m : rate);
        }
    }
}
