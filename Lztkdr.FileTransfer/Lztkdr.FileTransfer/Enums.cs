﻿namespace Lztkdr.FileTransfer
{
    /// <summary>
    /// 工作状态
    /// </summary>
    public enum State
    {
        等待 = 0,
        执行中 = 1,
        暂停 = 2,
        继续 = 3,
        重做 = 4,
        取消 = 5,
        出错 = 6,
        校验文件 = 99,
        完成 = 7,
    }
}
