﻿using System;
using Lztkdr.FileTransfer.Event;

namespace Lztkdr.FileTransfer
{
    /// <summary>
    /// 多任务(文件) 断点续传 类
    /// 外部注册，内部调用事件：OnStart 
    /// 外部注册，外部调用事件：OnUpdateProgress，OnError，OnCompleted
    /// </summary>
    public class MultiTaskTransfer
    {
        public event EventHandler<OnBigStartEventArgs> OnStart;

        public event EventHandler<OnBigUpdateProgressEventArgs> OnUpdateProgress;

        public event EventHandler<OnBigErrorEventArgs> OnError;

        public event EventHandler<OnBigCompletedEventArgs> OnCompleted;



        /// <summary>
        /// 一般存储 用于传输任务的参数对象集合
        /// </summary>
        public object UserState { get; set; }


        public MultiTaskTransfer()
        {

        }

        public MultiTaskTransfer(object userState = null)
        {
            this.UserState = userState;
        }

        public void Start()
        {
            this.OnStart?.Invoke(this, new OnBigStartEventArgs(this.UserState));
        }
    }
}
