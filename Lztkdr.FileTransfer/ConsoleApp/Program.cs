﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            
        }

        static byte[] GetFileBytes(string file, int start, int end)
        {
            FileStream fs = File.OpenRead(file);
            fs.Position = start;
            byte[] bs = new byte[end - start + 1];
            fs.Read(bs, 0, bs.Length);
            fs.Close();
            return bs;
        }

        static string Md5Hash(byte[] buffer)
        {
            MD5 md5Hash = MD5.Create();
            byte[] data = md5Hash.ComputeHash(buffer);
            StringBuilder builder = new StringBuilder();
            for (int j = 0; j < data.Length; j++)
            {
                builder.Append(data[j].ToString("x2"));
            }
            return builder.ToString();
        }
    }
}
